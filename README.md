# Marbotic Test Technique

## Introduction

En réponse à ma candidature pour leur offre de stage ["Stage Développeur Unity"](https://emploi.afjv.com/emploi-jeux-video/SDEV1190-17072),
Marbotic a proposé un test technique centré sur la [création d'une interface 2D de sélection de personnages](https://www.dropbox.com/sh/tdqfch30fw7pi5y/AAAHvFLu321tbHPdXiJ58Pgea?dl=0&preview=Test+Marbotic.pdf) afin d'évaluer mes capacités d'utilisation du moteur Unity. Le sujet de ce test m'a été dévoilé
le Vendredi 17 Janvier 2020, pour un rendu attendu le Lundi 20 Janvier 2020. Au cours du weekend, j'ai donc tenté au mieux de mes capacités de répondre aux exigences de Marbotic et du sujet en réalisant le projet disponible sur ce dépôt git.

Ce test aura été l'occasion de manipuler massivement les outils spécifiques à la création d'interface utilisateur (UI) de Unity, bien plus que ce que j'ai pu réaliser personnellement sur des petits projets personnels. Sur le plan personnel, ce test était donc une très bonne expérience malgré les difficultés, et je remercie Marbotic de me l'avoir proposé.

## Travail Réalisé

La consigne principale du sujet était la suivante :
>  Réaliser une interface 2D de sélection de personnages
Le sujet était ensuite divisé en sous-tâches afin de guider la réalisation de l'interface. Ces tâches, toutes abordés mais pas forcément réalisés, sont les suivantes :

- Créer un écran permettant d’afficher les caractéristiques d’un personnage
- Créer un écran permettant de parcourir les personnages
- Créer un écran permettant d’ajouter des personnages
- Créer un écran permettant de modifier les informations d’un personnage
- Ajouter la possibilité de supprimer un personnage (voire plusieurs personnages à la fois)
- Utiliser le fichier JSON pour afficher la liste de personnages
- Créer un Script Editor qui permet de consulter le nom de chaque personnage contenu dans le fichier JSON

Après une première lecture du sujet, il apparait que certaines de ces tâches peuvent être regroupés ensembles :
- Afficher les caractéristiques d’un personnage et pouvoir parcourir les personnages sont des sous-tâche d'un **écran de sélection de personnages**
- Supprimer, Modifier et Ajouter des personnages sont toutes des sous-tâches d'un **écran d'édition de personnages**
- La **gestion d'un fichier** permet de créer un lien entre les deux écrans précédents du point de vue du joueur, mais également de faciliter la création du jeu du point de vue du programmeur, à l'aide du **Script Editor**

C'est la démarche que ce test technique a tenté de suivre, en se concentrant tout d'abord sur la réalisation de l'écran de sélection.

### Création d'un écran de sélection de personnage

Cette partie est celle dont la réalisation est la plus poussés dans le test technique. Son développement a également conditionné la manière de concevoir la réponse aux autres tâches, puisqu'une bonne partie de l'interface et du code entre cet écran et celui d'édition sont similaires. Dans un premier temps une recherche d'ergonomie et de design a été faite, ce qui a amené à essayé de réaliser une interface similaire à celle de [Super Smash Bros. Ultimate](https://fr.wikipedia.org/wiki/Super_Smash_Bros._Ultimate) puis à celle de [Street Fighter](https://fr.wikipedia.org/wiki/Street_Fighter_(s%C3%A9rie)).

![Inspiration pour la première version de l'écran de sélection de personnages](gitImages/characterSelectionScreenReferences.png "Inspiration pour la première version de l'écran de sélection de personnages")

Cette idée a ensuite été abandonnée car la prise en compte du paramètre multijoueurs rendait la tâche difficile. Il a été alors décidé de réalisé une interface plus classique, en s'inspirant encore une fois de Street Fighter mais également des interfaces utilisés par les logiciels de chat vocaux et textuels tel que [Discord](https://fr.wikipedia.org/wiki/Discord_(logiciel) ou [Steam](https://fr.wikipedia.org/wiki/Steam).

![Version finale de l'écran de sélection de personnages](gitImages/characterSelectionScreenFinalVersion.png "Version finale de l'écran de sélection de personnages")

L'interface finale est composé de 3 grands Panels :
- A gauche, la liste complète des personnages du jeux, chacun d'entre eux représenté par un Button. Cette liste est générée lors du chargmenent de la scène. Un Button permet également de revenir au menu principal (voir plus bas). 
- Cliquer sur un des Buttons de personnage permet de modifier les champs du Panel à droite de l'interface. S'affiche alors le nom, l'age, la force et la vitesse du personnage, ainsi que sa liste de combos. Un emplacement supplémentaire à été rajouté pour afficher un portrait du personnage, mais n'a pas été utilisé.
- Enfin, le Panel du milieu peut servir à afficher un Sprite, voir un Modèle 3D du personnage. Cette partie n'a pas été implémenté en réel dans le test, mais afficher une représentation des personnages semblait un élément essentiel de tout bon écran de sélection. Ce Panel sert également a "lancer" le jeu, bien que pour le test technique appuyer sur le Button ne permette que d'afficher un texte spécifiant quel personnage l'utilisateur a choisi.

![Un exemple d'utilisation de l'interface de l'écran de sélection de personnages](gitImages/characterSelectionScreenExample.png "Un exemple d'utilisation de l'interface de l'écran de sélection de personnages")

La quasi totalité de l'interface est à base de Buttons : cette décision a été prise pour 2 raisons : la possibilité d'améliorer l'interface, puisque chaque élément est cliquable, en proposant des textes indicatifs par exemple (cette fonctionnalité se révélera très pratique lors de la création de l'écran d'édition); Et une incompréhension sur le comportement des zones de textes de l'UI d'Unity qui ne se plaçait pas correctement - à l'inverse des Buttons, un problème qui n'a pas été plus approfondie à cause de la contrainte temporelle du test technique.

| Fait / Fonctionnel | A Faire / A Améliorer |
|-----|----------|
| **Afficher les personnages** Au lancement de la scène Unity le script génère dynamiquement autant de Buttons que de personnages dans la liste des personnages. Du point de vue du joueur chaque Button est associé à un personnage.<br><br>**Afficher les caractéristiques d'un personnage** Lors d'un clic sur un des Buttons de la liste des personnages, les informations du personnage associé à ce Button sont affichés à droite de l'interface. Ces informations contiennent des données "statiques" (nom, age, vitesse et force) ainsi que sa liste de combos, qui est généré dynamiquement en fonction du nombre de combos du personnage. Toutes ces informations "effacent" les données précédemments affichés à ces emplacements (si le joueur avait sélectionné un autre personnage précédemment).<br><br>**Conservation des données de combos** Lorsqu'un personnage est choisi pour la première fois par le joueur, des Buttons sont créés afin de représenter sa liste de combos. Si le joueur choisi ensuite un autre personnage, afin d'éviter des fuites de données ses Buttons ne sont pas détruit : ils sont à la place cachés et aucunes interactions ne peut être réalisées avec eux. Cette procédure en 2 temps (cachés les combos de l'ancien personnage sélectionné / afficher (ou créer) les combos du nouveau personnage sélectionné) permet d'économiser de la place en mémoire ainsi que d'éviter des erreurs.<br><br>**Trace en mémoire du personnage sélectionné** En plus des combos, une référence au dernier personnage sélectionné (actuel du point de vue du joueur) est conservé et modifié en fonction des actions du joueur. Cela permet de simuler un lancement de jeu avec le bon personnage. | **Lien avec le fichier JSON** Comme décrit plus bas, les difficultés rencontrées lors du traitement du fichier JSON n'ont pas permis d'avoir une liste de personnages directement tiré du fichier.<br><br>**Meilleure gestion des liens personnage / Buttons** Bien que l'écran d'édition soit en grande partie similaire à celui de sélection, il profite d'une légère meilleure gestion des associations entre personnage (au sens instance de la Classe Character) et élément d'UI (Button de sélection, Buttons de combos). Il faudrait donc repasser sur le code de l'écran de sélection afin d'améliorer ici aussi ces liens - en effet pour l'instant par exemple le Button de sélection n'est pas vraiment associé à un personnage : au moment du clic une recherche dans la liste des personnages est alors nécessaire pour retrouver le personnage l'instance auquel on a "associé" le Button, au travers de son nom |

### Création d'un écran d'édition de personnage

Cette deuxième partie a hérité du travail fait sur la partie précédente : la majeure partie de l'interface et du code est en effet similaire entre les deux interfaces, ce qui peut être vu comme un avantage, permettant à l'utilisateur de retrouver ses marques si par exemple après avoir joué au jeu, il souhaite se lancer dans des modifications. On y retrouve les 3 panels de l'interface précédente, avec quelques modifications. La plus notable est l'apparition d'un bouton "Delete Selection" et l'apparition d'une cache à cocher sur les boutons de la liste des personnages. Ces deux nouveaux éléments de l'interface permettent de sélectionner puis de supprimer un ou plusieurs personnages de la liste des personnages en une seule fois, répondant ainsi à une des consignes du test technique. Un panel de confirmation - pour éviter les suppressions non désirés, vient compléter cette fonctionnalité de l'éditeur.

![Suppression d'un personnage de la liste](gitImages/characterEditionSimpleDeletion.png "Suppression d'un personnage de la liste")

![Suppression de plusieurs personnages de la liste](gitImages/characterEditionMultipleDeletion.png "Suppression de plusieurs personnages de la liste")

Autre différence, les boutons de caractéristiques sont désormais dotés d'une fonction, puisqu'ils permettent de modifier les champs du personnage : nom, âge, force, et vitesse. Le fait d'avoir construit la première interface à l'aide de Buttons c'est relevé utile ici puisqu'un temps considérable à été gagné ici. Cette fonctionnalité est également accompagnée d'un nouveau panel, permettant à l'utilisateur de rentrer la valeur du champ qu'il souhaite modifier, puis de la valider ou de l'annuler en cas d'erreur. La valeur rentrée par l'utilisateur est vérifiée au niveau du code, notamment au niveau de la longueur de la chaîne écrite : un meilleur contrôle serait par contre requis sur la vérification du type de la valeur (integer, float, string ...). A noter qu'un message permet de suivre le statut de la modification, en prévenant l'utilisateur si une erreur est survenue ou si sa modification a été accepté. Cette idée, arrivée un peu tard, n'aura pas eu le temps d'être étendue à toute l'interface. La liste des combos par contre n'est pas modifiable, par manque de temps. De même, la possibilité de créer de nouveaux personnages n'a pas été implémenté, pour la même raison et aussi parce que justement, la liste des combos n'était pas modifiable.

![Modification du champ "Name" du personnage Shakira](gitImages/characterEditionAttributeModification.png "Modification du champ 'Name' du personnage Shakira")

| Fait / Fonctionnel | A Faire / A Améliorer |
|-----|----------|
| **Supprimers des personnages** Une petite modification des Buttons des personnages ainsi que l'ajout d'un Button permettent de supprimer autant de personnages que l'utilisateur souhaite de la liste des personnages.<br><br>**Modifier les caractéristiques d'un personnage** Lors d'un clic sur un des Buttons de la liste des personnages, les informations du personnage associé à ce Button sont affichés à droite de l'interface. Les informations "statiques" (nom, age, vitesse et force) sont alors modifiables par simple clic sur les Buttons. Le programme empêche l'utilisateur de rentrer de trop longue chaînes, et lui confirme la modifiation grâce à un message affiché en bas de l'interface.<br><br> | **Lien avec le fichier JSON** Comme décrit plus bas, les difficultés rencontrées lors du traitement du fichier JSON n'ont pas permis d'avoir une liste de personnages directement tiré du fichier.<br><br>**Modification des combos** Bien qu'il soit possible de modifier les attributs d'un personnage, il n'est pas possible d'en faire de même pour sa liste de combo.<br><br>**Création de nouveaux personnages** Par manque de temps et problèmes rencontrés sur la gestion de la liste des combos, il n'est pas possible de créer de nouveaux personnages à partir de zéro et de les ajouter à la liste des personnages du jeu. |

### Gestion du fichier JSON

Le sujet du test technique était fourni avec un fichier JSON contenant des informations sur des personnages pré-créés. Ce fichier JSON possède la structure suivante :
```
| Racine
    | Characters (liste)
        | Character
            | Name
            | Age
            | Strengh
            | Speed
            | Combos (liste)
                | Combo
                    | Name
                    | Movement (liste)
```
On remarque directement que ce fichier JSON contient beaucoup de type complexe, avec des Objects et surtout des Arrays. Hors, [la classe implémentée de base dans Unity](https://docs.unity3d.com/ScriptReference/JsonUtility.html) pour gérer le JSON ne gère pas ces types complexes, comme précisé dans la documentation : 
> Unity does not support passing other types directly to the API, such as primitive types or arrays. If you need to convert those, wrap them in a class or struct of some sort.

La documentation ne fournissant pas de solution alternative, une recherche à été nécessaire pour trouver un module JSON qui non seulement pouvait traité les Arrays, mais également était compatible avec Unity. Une première solution trouvé est [JsonHelper](https://gist.github.com/halzate93/77e2011123b6af2541074e2a9edd5fc0), une classe essayant de wrapper les données JSON afin d'en extraire correctement les Arrays. Malheureusement après quelques essais, cette solution ne permettait pas de récupérer plus d'une couche d'Arrays : ainsi les Combos des personnages étaient toujours inacessibles.

Une autre solution a donc été choisie, [SimpleJson](http://wiki.unity3d.com/index.php/SimpleJSON). Cette solution permettait en quelques lignes, de récupérer la totalité du fichier JSON dans une structume custom. Si la présence des données fut bien confirmé, il a été impossible dans le délai impartie d'automatiser la récupération de ses données pour instancier nos objets des classes Character et Combo, puis les stocker dans une structure utilisée par le programme. Ainsi, le fichier JSON n'a pas pu être utilisé dans le cadre de ce test technique : une fausse liste de personnages à donc été créée "à la main" afin de réaliser les tests de l'interface.

### Création d'un Script Editor

Un Script Editor permet aux développeurs utilisants Unity de modifier l'interface du moteur pour y intégrer des outils 

## Références

La plupart des références portent sur l'utilisation de librairies de parsing du JSON compatible avec Unity, l'autre grosse partie étant elle consacré à l'UI, notamment les scrollbars et la modification des évènements associés aux Buttons (méthode **onClick**) et des Toggles (méthode **onValueChanged**) du fait de la méthode choisie pour gérer ces deux éléments. Le reste des références est consacré au Script Editor - malgré le fait que sa programmation est finalement été laissé de côté, ainsi que la documentation des Dictionary.

### JSON

- https://docs.unity3d.com/ScriptReference/JsonUtility.html
- https://stackoverflow.com/questions/36239705/serialize-and-deserialize-json-and-json-array-in-unity 
 -https://gist.github.com/halzate93/77e2011123b6af2541074e2a9edd5fc0
- https://www.codeproject.com/Articles/1201466/Working-with-JSON-in-Csharp-VB#simple_collection_types
- https://docs.microsoft.com/fr-fr/dotnet/standard/serialization/system-text-json-how-to
- http://wiki.unity3d.com/index.php/SimpleJSON
- https://github.com/Bunny83/SimpleJSON

### Elements UI

- https://www.youtube.com/watch?v=lUun2xW6FJ4
- https://answers.unity.com/questions/828666/46-how-to-get-name-of-button-that-was-clicked.html
- https://answers.unity.com/questions/1448790/change-onclick-function-via-script.html
- https://forum.unity.com/threads/set-toggles-onvaluechanged-function-in-script.313461/
- https://answers.unity.com/questions/902399/addlistener-to-a-toggle.html
- https://stackoverflow.com/questions/28273062/get-text-from-input-field-in-unity3d-with-c-sharp

### Script Editor 

- https://docs.unity3d.com/Manual/editor-EditorWindows.html
- https://www.youtube.com/watch?v=491TSNwXTIg

### Collection

- https://docs.microsoft.com/fr-fr/dotnet/api/system.collections.generic.dictionary-2?view=netframework-4.8
