﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SelectionScript : MonoBehaviour
{	
	public List<Character> characterList;
	
	public Transform CharacterSelectionButton;
	public Transform ComboButton;
	
	// Selected Character
	Character SC;
	
	// CaracteristicPanel
	private GameObject CP;
	private CanvasGroup CG;	
	private bool CharacteristicPanelOpen;
	
	// ComboListPanel
	private GameObject CLP;
	// Combo buttons list per character
	private Dictionary<int, List<Transform>> CBL;
	
	// GameLaunch Panel & PlayButton
	private GameObject GLP;
	private CanvasGroup cg_GLP;
	private GameObject PB;
	private CanvasGroup cg_PB;
	
    // Start is called before the first frame update
    void Start()
    {
        // Get characters information
		// TODO
		characterList = new List<Character>();
		CBL = new Dictionary<int, List<Transform>>();
		
		Character c1 = new Character();
		c1.SetID(1);		
		c1.name = "Shakira";
		c1.age = 16;
		c1.speed = 3.3f;
		c1.strengh = 10f;
		Movement c1m1 = new Movement();
		c1m1.input = new string[] {"A", "A", "X"};
		Combo c1_1 = new Combo();		
		c1_1.name = "Muda Muda Muda";
		c1_1.movements = c1m1;
		c1.combos = new Combo[] {c1_1};
		
		Character c2 = new Character();
		c2.SetID(2);
		c2.name = "Michael Le Normand";
		c2.age = 54;
		c2.speed = 12.1f;
		c2.strengh = 200f;
		Movement c2m1 = new Movement();
		c2m1.input = new string[] {"B", "A", "Up"};
		Combo c2_1 = new Combo();
		c2_1.name = "Super Brawler Move";
		c2_1.movements = c2m1;
		Movement c2m2 = new Movement();		
		c2m2.input = new string[] {"Up", "Down", "Up"};
		Combo c2_2 = new Combo();
		c2_2.name = "Babushka's Mayonez";
		c2_2.movements = c2m2;
		c2.combos = new Combo[] {c2_1, c2_2};
		
		
		characterList.Add(c1);
		characterList.Add(c2);
		
		SC = c1;
		
		// Set UI
		CP = GameObject.FindWithTag("CharacterCaracteristicsPanel");
		CLP = GameObject.FindWithTag("CharCarac_ComboList");
		GLP = GameObject.FindWithTag("LaunchGamePanel");
		PB = GameObject.FindWithTag("PlayButton");
		
		// Create as many buttons as characters
		GameObject UICharacterList = GameObject.FindWithTag("CharSelect_CharList");
		foreach (Character c in characterList)
		{
			Transform csb = Instantiate(CharacterSelectionButton);
			csb.gameObject.GetComponentInChildren<Text>().text = c.name;
			csb.SetParent(UICharacterList.transform);
			csb.GetComponent<Button>().onClick.AddListener(SelectCharacter);
		}
		
		// Hide the Caracteristics Panel
		CharacteristicPanelOpen = true;
		CG = CP.GetComponent<CanvasGroup>();
		HideCaracteristicsPanel();
		
		// Hide the playButton and Launch Game Panel
		cg_GLP = GLP.GetComponent<CanvasGroup>();
		cg_PB = PB.GetComponent<CanvasGroup>();
		HidePlayButton();
		HideLaunchGamePanel();
	}
	
	// --- UI SCRIPTS --- //
	public void ShowCaracteristicsPanel()
	{
		if (!CharacteristicPanelOpen)
		{
			CG.alpha = 100f;
			CG.interactable = true;
			CG.blocksRaycasts = true;
			CharacteristicPanelOpen = true;
		}
	}
	
	public void HideCaracteristicsPanel()
	{
		if (CharacteristicPanelOpen)
		{
			CG.alpha = 0f;
			CG.interactable = false;
			CG.blocksRaycasts = false;
			CharacteristicPanelOpen = false;
		}
	}
	
	public void ShowPlayButton()
	{
		cg_PB.alpha = 100f;
		cg_PB.interactable = true;
		cg_PB.blocksRaycasts = true;
		PB.GetComponent<Button>().onClick.AddListener(ShowLaunchGamePanel);
	}
	
	public void HidePlayButton()
	{
		cg_PB.alpha = 0f;
		cg_PB.interactable = false;
		cg_PB.blocksRaycasts = false;
	}
	
	public void ShowLaunchGamePanel()
	{
		cg_GLP.alpha = 100f;
		cg_GLP.interactable = true;
		cg_GLP.blocksRaycasts = true;
		GameObject.FindWithTag("ChoosenCharacName").GetComponent<Text>().text = SC.name;
	}
	
	public void HideLaunchGamePanel()
	{
		cg_GLP.alpha = 0f;
		cg_GLP.interactable = false;
		cg_GLP.blocksRaycasts = false;
	}
	
	public void SelectCharacter()
	{
		ShowCaracteristicsPanel();
		GameObject button = EventSystem.current.currentSelectedGameObject;
		string t = button.GetComponentInChildren<Text>().text;
		Debug.Log(t);
		
		// Hide old selected character information (combo list & launch panel & play button)		
		HideCharacterComboButton(SC);
		HideLaunchGamePanel();
		HidePlayButton();
		
		// Get back character information
		SC = characterList.Find(x => x.name == t);
		string name = SC.name;
		string age = SC.age.ToString();
		string strengh = SC.strengh.ToString();
		string speed = SC.speed.ToString();
		
		// Replace information in the caracteristics panel
		GameObject.FindWithTag("CharCarac_Name").GetComponentInChildren<Text>().text = "Name : "+name;
		GameObject.FindWithTag("CharCarac_Age").GetComponentInChildren<Text>().text = "Age : "+age;
		GameObject.FindWithTag("CharCarac_Strengh").GetComponentInChildren<Text>().text = "Strengh : "+strengh;
		GameObject.FindWithTag("CharCarac_Speed").GetComponentInChildren<Text>().text = "Speed : "+speed;
		
		// Create combo list
		ShowCharacterComboButton(SC);
		
		// Show play button
		ShowPlayButton();
		
		
	}
	
	private void ShowCharacterComboButton(Character c)
	{
		if (CBL.ContainsKey(c.GetID()))
		{
			//Debug.Log("show [OUI] : "+c.GetID());
			foreach(Transform t in CBL[c.GetID()])
			{
				t.SetParent(CLP.transform);
				ShowComboButton(t);
			}
		}
		else
		{
			//Debug.Log("show [NON] : "+c.GetID());
			CreateCharacterComboButton(c);
		}
	}
	
	private void HideCharacterComboButton(Character c)
	{
		if (CBL.ContainsKey(c.GetID()))
		{
			//Debug.Log("hide [OUI] : "+c.GetID());
			foreach(Transform t in CBL[c.GetID()])
			{
				t.SetParent(null);
				HideComboButton(t);
			}
		}
	}
	
	private void CreateCharacterComboButton(Character c)
	{
		List<Transform> l = new List<Transform>();
		
		foreach (Combo com in c.combos)
		{
			Transform cb = Instantiate(ComboButton);
			cb.gameObject.GetComponentInChildren<Text>().text = com.name+"\n"+com.movements.ToString();
			cb.SetParent(CLP.transform);
			l.Add(cb);
		}
		
		CBL.Add(c.GetID(), l);
	}
	/*
	private void DestroyCharacterComboButton()
	{
		foreach (Transform t in CBL)
		{
			Destroy(t.gameObject);
		}
	}*/
	
	private void ShowComboButton(Transform t)
	{
		CanvasGroup c = t.gameObject.GetComponent<CanvasGroup>();
		c.alpha = 100f;
		c.interactable = true;
		c.blocksRaycasts = true;
	}
	
	private void HideComboButton(Transform t)
	{
		CanvasGroup c = t.gameObject.GetComponent<CanvasGroup>();
		c.alpha = 0f;
		c.interactable = false;
		c.blocksRaycasts = false;
	}
	
	public void BackToMenu()
	{
		SceneManager.LoadScene("Menu");
	}
}
