﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class UIMenuScript : MonoBehaviour
{
	
	public void LaunchGameCharacterSelection()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}
	
	public void LaunchGameCharacterEditor()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
	}
	
	public void Quit()
	{
		Application.Quit();
	}
}
