﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class EditorScript : MonoBehaviour
{	
	public List<Character> characterList;
	
	public Transform CharacterEditionButton;
	public Transform ComboButton;
	
	// Selected Character
	Character SC;
	
	// CaracteristicPanel
	private GameObject CP;
	private CanvasGroup CG;	
	private bool CharacteristicPanelOpen;
	
	// ComboListPanel
	private GameObject CLP;
	// Combo buttons list per character
	private Dictionary<int, List<Transform>> CBL;
	
	// Character button associated to character
	private Dictionary<int, Transform> CBC;
	
	// Edition Panel & Confirm Deletion Panel
	private GameObject EP;
	private CanvasGroup cg_EP;
	private GameObject CDP;
	private CanvasGroup cg_CDP;
	
	// status variables
	private List<Character> characterToDelete;
	private bool saved;	// not used
	private string valueModified;
	
	public int minNameLength = 1;
	public int maxNameLength = 20;
	public int minAgeValue = 0;
	public int maxAgeValue = Int32.MaxValue;
	public float minSpeedValue = 0f;
	public float maxSpeedValue = 100f;
	public float minStrenghValue = 0f;
	public float maxStrenghValue = 100f;
	
    // Start is called before the first frame update
    void Start()
    {
        // Get characters information
		// TODO
		characterList = new List<Character>();
		characterToDelete = new List<Character>();
		CBL = new Dictionary<int, List<Transform>>();
		CBC = new Dictionary<int, Transform>();
		
		Character c1 = new Character();
		c1.SetID(1);		
		c1.name = "Shakira";
		c1.age = 16;
		c1.speed = 3.3f;
		c1.strengh = 10f;
		Movement c1m1 = new Movement();
		c1m1.input = new string[] {"A", "A", "X"};
		Combo c1_1 = new Combo();		
		c1_1.name = "Muda Muda Muda";
		c1_1.movements = c1m1;
		c1.combos = new Combo[] {c1_1};
		
		Character c2 = new Character();
		c2.SetID(2);
		c2.name = "Michael Le Normand";
		c2.age = 54;
		c2.speed = 12.1f;
		c2.strengh = 200f;
		Movement c2m1 = new Movement();
		c2m1.input = new string[] {"B", "A", "Up"};
		Combo c2_1 = new Combo();
		c2_1.name = "Super Brawler Move";
		c2_1.movements = c2m1;
		Movement c2m2 = new Movement();		
		c2m2.input = new string[] {"Up", "Down", "Up"};
		Combo c2_2 = new Combo();
		c2_2.name = "Babushka's Mayonez";
		c2_2.movements = c2m2;
		c2.combos = new Combo[] {c2_1, c2_2};
		
		
		characterList.Add(c1);
		characterList.Add(c2);
		
		SC = c1;
		
		// Set UI
		CP = GameObject.FindWithTag("CharacterCaracteristicsPanel");
		CLP = GameObject.FindWithTag("CharCarac_ComboList");
		EP = GameObject.FindWithTag("EditionPanel");
		CDP = GameObject.FindWithTag("ConfirmDeletionPanel");
		
		// Create as many buttons as characters
		ShowCharacterListButton();
		/*
		GameObject UICharacterList = GameObject.FindWithTag("CharSelect_CharList");
		foreach (Character c in characterList)
		{
			Transform csb = Instantiate(CharacterEditionButton);
			csb.gameObject.GetComponentInChildren<Text>().text = c.name;
			csb.SetParent(UICharacterList.transform);
			csb.GetComponent<Button>().onClick.AddListener(SelectCharacter);
			csb.GetComponentInChildren<Toggle>().onValueChanged.AddListener((value) => ToggleDeletionList(value));
		}
		*/
		// Hide the Caracteristics Panel
		CharacteristicPanelOpen = true;
		CG = CP.GetComponent<CanvasGroup>();
		HideCaracteristicsPanel();
		
		// Hide the Edition and Confirm Deletion Panels
		cg_EP = EP.GetComponent<CanvasGroup>();
		cg_CDP = CDP.GetComponent<CanvasGroup>();
		HideEditionPanel();
		HideConfirmDeletionPanel();
		
		valueModified = "";
	}
	
	// --- UI SCRIPTS --- //
	public void ShowCaracteristicsPanel()
	{
		if (!CharacteristicPanelOpen)
		{
			CG.alpha = 100f;
			CG.interactable = true;
			CG.blocksRaycasts = true;
			CharacteristicPanelOpen = true;
		}
	}
	
	public void HideCaracteristicsPanel()
	{
		if (CharacteristicPanelOpen)
		{
			CG.alpha = 0f;
			CG.interactable = false;
			CG.blocksRaycasts = false;
			CharacteristicPanelOpen = false;
		}
	}
	
	public void ShowEditionPanel()
	{
		cg_EP.alpha = 100f;
		cg_EP.interactable = true;
		cg_EP.blocksRaycasts = true;
	}
	
	public void HideEditionPanel()
	{
		cg_EP.alpha = 0f;
		cg_EP.interactable = false;
		cg_EP.blocksRaycasts = false;
	}
	
	public void ShowConfirmDeletionPanel()
	{
		if (characterToDelete.Count > 0)
		{
			cg_CDP.alpha = 100f;
			cg_CDP.interactable = true;
			cg_CDP.blocksRaycasts = true;
			GameObject.FindWithTag("ConfirmDeletionText").GetComponent<Text>().text = "Do you want to delete "+characterToDelete.Count+" character(s) ?";
		}
	}
	
	public void HideConfirmDeletionPanel()
	{
		cg_CDP.alpha = 0f;
		cg_CDP.interactable = false;
		cg_CDP.blocksRaycasts = false;
	}
	
	public void SelectCharacter()
	{
		HideEditionPanel();
		ShowCaracteristicsPanel();
		GameObject button = EventSystem.current.currentSelectedGameObject;
		string t = button.GetComponentInChildren<Text>().text;
		//Debug.Log(t);
		
		// Hide old selected character information (combo list & launch panel & play button)		
		HideCharacterComboButton(SC);
		//HideLaunchGamePanel();
		//HidePlayButton();
		
		// Get back character information
		SC = characterList.Find(x => x.name == t);
		string name = SC.name;
		string age = SC.age.ToString();
		string strengh = SC.strengh.ToString();
		string speed = SC.speed.ToString();
		
		// Replace information in the caracteristics panel
		GameObject.FindWithTag("CharCarac_Name").GetComponentInChildren<Text>().text = "Name : "+name;
		GameObject.FindWithTag("CharCarac_Age").GetComponentInChildren<Text>().text = "Age : "+age;
		GameObject.FindWithTag("CharCarac_Strengh").GetComponentInChildren<Text>().text = "Strengh : "+strengh;
		GameObject.FindWithTag("CharCarac_Speed").GetComponentInChildren<Text>().text = "Speed : "+speed;
		
		// Create combo list
		ShowCharacterComboButton(SC);
		
		// Show play button
		//ShowPlayButton();
		
		
	}
	
	private void ShowCharacterListButton()
	{
		GameObject UICharacterList = GameObject.FindWithTag("CharSelect_CharList");
		foreach (Character c in characterList)
		{
			if (CBC.ContainsKey(c.GetID()))
			{
				CBC[c.GetID()].SetParent(UICharacterList.transform);
				CBC[c.GetID()].gameObject.GetComponentInChildren<Text>().text = c.name;
				ShowCharacterListButton(CBC[c.GetID()]);
			}
			else
			{
				Transform csb = Instantiate(CharacterEditionButton);
				csb.gameObject.GetComponentInChildren<Text>().text = c.name;
				csb.SetParent(UICharacterList.transform);
				csb.GetComponent<Button>().onClick.AddListener(SelectCharacter);
				csb.GetComponentInChildren<Toggle>().onValueChanged.AddListener((value) => ToggleDeletionList(value));
				CBC.Add(c.GetID(), csb);
			}
		}
	}
	
	private void HideCharacterListButton()
	{
		foreach (Character c in characterList)
		{
			if (CBC.ContainsKey(c.GetID()))
			{
				CBC[c.GetID()].SetParent(null);
				HideCharacterListButton(CBC[c.GetID()]);
			}
		}
	}
	
	private void DestroyCharacterListButton()
	{
		foreach(Character c in characterToDelete)
		{
			if (CBC.ContainsKey(c.GetID()))
			{
				CBC[c.GetID()].SetParent(null);
				HideCharacterListButton(CBC[c.GetID()]);
				Transform dead = CBC[c.GetID()];
				CBC.Remove(c.GetID());
				Destroy(dead.gameObject);
			}
		}
	}
	
	private void ShowCharacterListButton(Transform t)
	{
		CanvasGroup c = t.gameObject.GetComponent<CanvasGroup>();
		c.alpha = 100f;
		c.interactable = true;
		c.blocksRaycasts = true;
	}
	
	private void HideCharacterListButton(Transform t)
	{
		CanvasGroup c = t.gameObject.GetComponent<CanvasGroup>();
		c.alpha = 0f;
		c.interactable = false;
		c.blocksRaycasts = false;
	}
	
	private void ShowCharacterComboButton(Character c)
	{
		if (CBL.ContainsKey(c.GetID()))
		{
			//Debug.Log("show [OUI] : "+c.GetID());
			foreach(Transform t in CBL[c.GetID()])
			{
				t.SetParent(CLP.transform);
				ShowComboButton(t);
			}
		}
		else
		{
			//Debug.Log("show [NON] : "+c.GetID());
			CreateCharacterComboButton(c);
		}
	}
	
	private void HideCharacterComboButton(Character c)
	{
		if (CBL.ContainsKey(c.GetID()))
		{
			//Debug.Log("hide [OUI] : "+c.GetID());
			foreach(Transform t in CBL[c.GetID()])
			{
				t.SetParent(null);
				HideComboButton(t);
			}
		}
	}
	
	private void CreateCharacterComboButton(Character c)
	{
		List<Transform> l = new List<Transform>();
		
		foreach (Combo com in c.combos)
		{
			Transform cb = Instantiate(ComboButton);
			cb.gameObject.GetComponentInChildren<Text>().text = com.name+"\n"+com.movements.ToString();
			cb.SetParent(CLP.transform);
			l.Add(cb);
		}
		
		CBL.Add(c.GetID(), l);
	}
	/*
	private void DestroyCharacterComboButton()
	{
		foreach (Transform t in CBL)
		{
			Destroy(t.gameObject);
		}
	}*/
	
	private void ShowComboButton(Transform t)
	{
		CanvasGroup c = t.gameObject.GetComponent<CanvasGroup>();
		c.alpha = 100f;
		c.interactable = true;
		c.blocksRaycasts = true;
	}
	
	private void HideComboButton(Transform t)
	{
		CanvasGroup c = t.gameObject.GetComponent<CanvasGroup>();
		c.alpha = 0f;
		c.interactable = false;
		c.blocksRaycasts = false;
	}
	
	public void BackToMenu()
	{
		SceneManager.LoadScene("Menu");
	}
	
	
	/* ----------------------------------------- */
	/* -------- EDITOR SPECIFIC METHODS -------- */
	/* ----------------------------------------- */
	
	/* ---- CHARACTER DELETION METHOS ---- */
	public void ToggleDeletionList(bool val)
	{
		//Debug.Log(val);
		GameObject toggle = EventSystem.current.currentSelectedGameObject;
		string t = toggle.transform.parent.gameObject.GetComponentInChildren<Text>().text;
		
		if (val)
		{
			AddToDeletionList(characterList.Find(x => x.name == t));
		}
		else
		{
			RemoveFromDeletionList(characterList.Find(x => x.name == t));
		}
	}
	
	public void AddToDeletionList(Character c)
	{
		//Debug.Log("true "+c.name);
		characterToDelete.Add(c);
		//Debug.Log(characterToDelete.Count);
	}
	
	public void RemoveFromDeletionList(Character c)
	{
		//Debug.Log("false "+c.name);
		characterToDelete.Remove(c);		
		//Debug.Log(characterToDelete.Count);
	}
	
	public void ConfirmCharactersDeletion()
	{
		// Clean first the buttons list
		DestroyCharacterListButton();

		foreach(Character c in characterToDelete)
		{
			characterList.Remove(c);
		}
		HideCaracteristicsPanel();
		HideConfirmDeletionPanel();
		HideCharacterListButton();
		ShowCharacterListButton();
		
		// Clean the list to be sure
		characterToDelete.Clear();
	}
	
	
	/* ---- CHARACTER MODIFICATION METHODS ---- */
	public void EditCharacterName()
	{
		HideEditionPanel();
		valueModified = "Name";
		GameObject.FindWithTag("ModifPanel_Text").GetComponent<Text>().text = "Enter the new ["+valueModified+"] of ["+SC.name+"]";
		ShowEditionPanel();
	}
	
	public void EditCharacterAge()
	{
		HideEditionPanel();
		valueModified = "Age";
		GameObject.FindWithTag("ModifPanel_Text").GetComponent<Text>().text = "Enter the new ["+valueModified+"] of ["+SC.name+"]";
		ShowEditionPanel();
	}
	
	public void EditCharacterSpeed()
	{
		HideEditionPanel();
		valueModified = "Speed";
		GameObject.FindWithTag("ModifPanel_Text").GetComponent<Text>().text = "Enter the new ["+valueModified+"] of ["+SC.name+"]";
		ShowEditionPanel();
	}
	
	public void EditCharacterStrengh()
	{
		HideEditionPanel();
		valueModified = "Strengh";
		GameObject.FindWithTag("ModifPanel_Text").GetComponent<Text>().text = "Enter the new ["+valueModified+"] of ["+SC.name+"]";
		ShowEditionPanel();
	}
	
	// Used mainly when the user did not enter a valid value for the attribute
	public void EditStatus(string text)
	{
		GameObject.FindWithTag("Editor_StatusText").GetComponent<Text>().text = text;
	}
	
	public void EditConfirmEdition()
	{
		GameObject inputField = GameObject.FindWithTag("ModifPanel_Input");		
		string content = inputField.GetComponent<Text>().text;	// TODO : use a Listener
		
		if (valueModified == "Name")
		{
			if ((content.Length >= minNameLength) && (content.Length <= maxNameLength)) // TODO : check spaces
			{
				SC.name = content;
				HideCharacterListButton();
				HideCaracteristicsPanel();
				ShowCharacterListButton();
				HideEditionPanel();
				EditStatus("New name applied");
			}
			else
			{
				EditStatus("The new name of the character must have a length between "+minNameLength+" and "+maxNameLength);
			}
		}
		
		if (valueModified == "Age")
		{
			int age = 0;
			
			if ((Int32.TryParse(content, out age)))
			{
				if ((age >= minAgeValue) && (age <= maxAgeValue))
				{
					SC.age = age;
					HideCharacterListButton();
					HideCaracteristicsPanel();
					ShowCharacterListButton();
					HideEditionPanel();
					EditStatus("New age applied");
				}
				else
				{
					EditStatus("The new age of the character must be between "+minAgeValue+" and "+maxAgeValue);
				}
			}
		}
		
		if (valueModified == "Strengh")
		{
			content = content.Replace(",", ".");
			float str = float.Parse(content, System.Globalization.CultureInfo.InvariantCulture);
			if ((str >= minStrenghValue) && (str <= maxStrenghValue))
			{
				SC.strengh = str;
				HideCharacterListButton();
				HideCaracteristicsPanel();
				ShowCharacterListButton();
				HideEditionPanel();
				EditStatus("New strengh applied");
			}
			else
			{
				EditStatus("The new strengh of the character must be between "+minStrenghValue+" and "+maxStrenghValue);
			}
		}
		
		if (valueModified == "Speed")
		{
			content = content.Replace(",", ".");
			float spd = float.Parse(content, System.Globalization.CultureInfo.InvariantCulture);
			if ((spd >= minSpeedValue) && (spd <= maxSpeedValue))
			{
				SC.speed = spd;
				HideCharacterListButton();
				HideCaracteristicsPanel();
				ShowCharacterListButton();
				HideEditionPanel();
				EditStatus("New speed applied");
			}
			else
			{
				EditStatus("The new speed of the character must be between "+minSpeedValue+" and "+maxSpeedValue);
			}
		}
	}
}
