﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.Text.Json;
//using System.Text.Json.Serialization;

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }
   
	public static Character[] FromCharacterJson(string json)
    {
        CharacterWrapper wrapper = JsonUtility.FromJson<CharacterWrapper>(json);
        return wrapper.characters;
    }
	
	public static Combo[] FromComboJson(string json)
    {
        ComboWrapper wrapper = JsonUtility.FromJson<ComboWrapper>(json);
        return wrapper.combos;
    }
	
    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [System.Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
	
	[System.Serializable]
    private class CharacterWrapper
    {
        public Character[] characters;
    }
	
	[System.Serializable]
    private class ComboWrapper
    {
        public Combo[] combos;
    }
}