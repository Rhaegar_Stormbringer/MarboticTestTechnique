﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Character
{
	private int ID = 0;
	public string name;
	public int age;
	public float speed;
	public float strengh;
	public Combo[] combos;
	
	public int GetID()
	{
		return ID;
	}
	
	public void SetID(int val)
	{
		// The ID can be set only once
		if (ID == 0)
		{
			ID = val;
		}
	}
}

[System.Serializable]
public class Combo
{
	public string name;
	public Movement movements;
}

[System.Serializable]
public class Movement
{
	public string[] input;
	
	public string ToString()
	{
		string s = "";
		foreach (string si in input)
		{
			s += si+" ";
		}
		return s;
	}
}